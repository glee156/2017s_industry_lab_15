package ictgradschool.industry.lab15.ex01;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.*;
import java.util.List;

/**
 * Created by cku782 on 8/01/2018.
 */
public class NestingShape extends Shape {

    List<Shape> childShapeList;

    public NestingShape() {
        super();
        this.childShapeList = new ArrayList<>();
    }

    public NestingShape(int x, int y) {
        super(x, y);
        this.childShapeList = new ArrayList<>();
    }

    public NestingShape(int x, int y, int deltaX, int deltaY) {
        super(x, y, deltaX, deltaY);
        this.childShapeList = new ArrayList<>();
    }

    public NestingShape(int x, int y, int deltaX, int deltaY, int height, int width) {
        super(x, y, deltaX, deltaY, height, width);
        this.childShapeList = new ArrayList<>();
    }


    public void move(int screenWidth, int screenHeight) {
        super.move(screenWidth, screenHeight);
        for (Shape a : childShapeList) {
            a.move(this.getWidth(), this.getHeight());
        }
    }

    public void add(Shape shape) throws IllegalArgumentException {
        if (shape.getParent() != null) {
            throw new IllegalArgumentException("The child already have a parent");
        }
        else if ((shape.getWidth() > fWidth) || (shape.getHeight() > fHeight)) {
            throw new IllegalArgumentException("Too big");
        }
        else if((shape.getWidth() + shape.getX()> fWidth) || (shape.getHeight() + shape.getY()> fHeight) ) {
            throw new IllegalArgumentException("Shape out of bounds");
            }
        else if((shape.getX()<0) || (shape.getY()<0)) {
            throw new IllegalArgumentException("Shape out of bounds");
        }

        else {
            shape.addParent(this);
            childShapeList.add(shape);
        }
    }

    public void remove(Shape shape) {
        if (!shape.getParent().equals(this)) {
            return;
        }
        childShapeList.remove(shape);
        shape.addParent(null);
    }

    public int shapeCount() {

        return childShapeList.size();
    }

    public int indexOf(Shape child) {
        for (int i = 0; i < childShapeList.size(); i++) {
            Shape a = childShapeList.get(i);
            if (a.equals(child)) {
                return i;
            }


        }
        return -1;
    }

    public boolean contains(Shape child) {
        for (int i = 0; i < childShapeList.size(); i++) {
            Shape a = childShapeList.get(i);
            if (a.equals(child)) {
                return true;
            }


        }
        return false;
    }

    @Override
    public void paint(Painter painter) {

        //drawing parent
        painter.drawRect(fX, fY, fWidth, fHeight);

        painter.translate(fX, fY);
        //drawing children
        for (Shape a : childShapeList) {
            a.paint(painter);
        }
        painter.translate(-fX,-fY);
    }

    public Shape shapeAt(int index){
        return childShapeList.get(index);

    }
}
