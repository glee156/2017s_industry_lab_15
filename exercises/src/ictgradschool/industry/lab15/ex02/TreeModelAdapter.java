package ictgradschool.industry.lab15.ex02;

import ictgradschool.industry.lab15.ex01.NestingShape;
import ictgradschool.industry.lab15.ex01.Shape;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by glee156 on 9/01/2018.
 */
public class TreeModelAdapter implements TreeModel{
    private NestingShape _adaptee;
    //private List<Shape> childrenShapes;

    public TreeModelAdapter (NestingShape root){
        this._adaptee=root;
        //this.childrenShapes = new ArrayList<Shape>();
    }

    @Override
    public Object getRoot() {
        return _adaptee;
    }

    @Override
    public Object getChild(Object parent, int index) {
        Shape result = null;
        if(parent instanceof NestingShape){
            NestingShape current = (NestingShape) parent;
            result = current.shapeAt(index);
        }
        return result;
    }

    @Override
    public int getChildCount(Object parent) {
        int result = 0;
        Shape shape= (Shape) parent;
        if (shape instanceof NestingShape){
            NestingShape parents= (NestingShape) shape;
            result= parents.shapeCount();

        }
        return result;
    }

    @Override
    public boolean isLeaf(Object node) {
        return (!(node instanceof NestingShape));
    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {
    //no implemenation required
    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {
        NestingShape newParent;
        Shape newChild;

        if(parent instanceof NestingShape && child instanceof Shape) {
            newParent = (NestingShape)parent;
            newChild = (Shape)child;
            return newParent.indexOf(newChild);
        }

        return 0;
    }


    @Override
    public void addTreeModelListener(TreeModelListener l) {

    }

    @Override
    public void removeTreeModelListener(TreeModelListener l){

    }

}



